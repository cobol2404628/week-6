       IDENTIFICATION DIVISION.
       PROGRAM-ID. READ5.
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT 100-INPUT-FILE ASSIGN TO "MEMBER.DAT"
              ORGANIZATION IS SEQUENTIAL
              FILE STATUS IS WS-INPUT-FILE-STATUS.
       DATA DIVISION. 
       FILE SECTION. 
       FD  100-INPUT-FILE
           BLOCK CONTAINS 0 RECORDS.
       01 100-INPUT-RECORD.
          05 MEMBER-ID          PIC   X(5).
          05 MEMBER-NAME        PIC   X(20).
          05 MEMBER-SECTION     PIC   9(1).
          05 MEMBER-GENDER      PIC   X(1).
          05 FILLER             PIC   X(53).

       WORKING-STORAGE SECTION. 
       01 WS-INPUT-FILE-STATUS  PIC X(2).
          88 FILE-OK                        VALUE '00'.
          88 FILE-AT-END                    VALUE '10'.
       01 WS-INPUT-COUNT        PIC 9(7)    VALUE ZERO.
       01 WS-MALE               PIC 9(7)    VALUE ZERO.
       01 WS-FEMALE             PIC 9(7)    VALUE ZERO.     
       01 WS-CHOICEL1           PIC 9(7)    VALUE ZERO.
       01 WS-CHOICEL2           PIC 9(7)    VALUE ZERO.
           
       PROCEDURE DIVISION.
       0000-MAIN-PROGRAM.
           PERFORM 1000-INITIAIL THRU 1000-EXIT
           PERFORM 2000-PROCESS THRU 2000-EXIT UNTIL FILE-AT-END 
           PERFORM 3000-END THRU 3000-EXIT.
           GOBACK. 


       1000-INITIAIL.
           OPEN INPUT 100-INPUT-FILE
           IF NOT FILE-OK 
              DISPLAY "FILE NOT FOUND!!!"
              STOP RUN
           END-IF.
           PERFORM 8000-READ THRU 8000-EXIT.
       1000-EXIT.
           EXIT.
      

       2000-PROCESS.
           DISPLAY WS-INPUT-FILE-STATUS
           DISPLAY 100-INPUT-RECORD
           ADD 1 TO WS-INPUT-COUNT.
           PERFORM 2100-COUNT-GENDER THRU 2100-EXIT 
           PERFORM 8000-READ THRU 8000-EXIT.
       2000-EXIT.
           EXIT.


       2100-COUNT-GENDER.
           IF MEMBER-GENDER = 'F'
              ADD 1 TO WS-FEMALE 
           ELSE
              ADD 1 TO WS-MALE 
           END-IF.
       2100-EXIT.
           EXIT.
       

       3000-END.
           CLOSE 100-INPUT-FILE
           DISPLAY WS-INPUT-FILE-STATUS
           DISPLAY "READ " WS-INPUT-COUNT
           DISPLAY "MALE " WS-MALE 
           DISPLAY "FEALE " WS-FEMALE  
           .
       3000-EXIT.
           EXIT.


       8000-READ.
           READ 100-INPUT-FILE.
       8000-EXIT.
           EXIT.